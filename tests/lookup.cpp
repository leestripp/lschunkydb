#include "lsChunkyDB.h"

int main( int argc, char *argv[] )
{
	lsChunkyDB data;
	// init returns 0 for success
	if( data.init( "test (test) <test@lschunkydb.app>", "test_data" ) )
	{
		cerr << "ERROR: Failed to initialize ChunkyDB" << endl;
		return 1;
	}
	
	// debug
	cout << "argc: " << argc;
	cout << " , argv[0]: " << argv[0] << endl;
	if( argc <= 1 )
	{
		cout << "Usage: lookup [option]" << endl;
		cout << "\t-s \"[prompt]\"\t : lookup a string of text." << endl;
		cout << "\t-d\t\t : delete data from disk." << endl;
		return 0;
	}
	
	// convert argv
	vector<string> args;
	for( int i=0; i<argc; i++  )
	{
		args.push_back( argv[i] );
	}
	
	// parce argv
	string prompt;
	if( args[1] == "-d" )
	{
		data.delete_data();
		return 0;
		
	} else if( args[1] == "-s" )
	{
		if( argc == 3 )
		{
			prompt = args[2];
		}
	} else
	{
		prompt = "What is an Orange?";
	}
	
	// check prompt
	if( prompt.empty() )
	{
		cerr << "ERROR: Empty prompt." << endl;
		return 1;
	}
	
	// load some test data if database is empty.
	if( data.empty() )
	{
		cout << "Empty database. Loading test data." << endl;
		
		// void insert( const string& data, ulong chunk_size );
		string orange( "Orange\n"
			"An orange is a round citrus fruit known for its bright orange color. It is typically sectioned into segments "
			"with juicy, sweet-tart pulp that is rich in vitamin C and dietary fiber. Oranges are usually eaten fresh but can "
			"also be used to make a variety of products such as juice, marmalade, and essential oil. They originated in Southeast "
			"Asia and have become a popular and widely cultivated fruit around the world due to their refreshing taste and nutritional value."
		);
		data.insert( orange, 1000 );
		
		string apple( "Apple\n"
			"An apple is a sweet, juicy fruit that grows on the apple tree (Malus domestica). It is native to Central Asia and "
			"is now cultivated in many parts of the world. Apples come in various sizes, shapes, and colors, with the most common "
			"being red or green when ripe. The fruit has a crisp, white flesh that surrounds the seeds in the center. Apples are "
			"rich in dietary fiber, vitamin C, and various antioxidants. They can be eaten fresh, used in cooking, or transformed "
			"into juice, cider, or other products. Apples are not only delicious but also offer numerous health benefits, making them "
			"a popular and versatile fruit."
		);
		data.insert( apple, 1000 );
	}
	
	// lookup tests.
	
	// vector<lsChunk> find_chunks( const string& prompt, ulong num_chunks );
	cout << "\nSearching for : " << prompt << endl;
	auto chunks = data.find_chunks( prompt, 4 );
	// Show results
	for( const auto& chunk : chunks )
	{
		cout << "### Result" << endl;
		cout << "ID : " << chunk.m_id << endl;
		cout << "Chunk : " << data.decrypt_chunk( chunk, true ) << endl;
		cout << "Reference : " << chunk.m_reference << endl;
	}
	
	// After cleanup() test
	cout << "\nCalling cleanup() : This only clears the Trie not the data on disk." << endl;
	data.cleanup();
	
	cout << "\nSearching for : " << prompt << endl;
	chunks = data.find_chunks( prompt, 4 );
	// Show results
	for( const auto& chunk : chunks )
	{
		cout << "### Result" << endl;
		cout << "ID : " << chunk.m_id << endl;
		cout << "Chunk : " << data.decrypt_chunk( chunk ) << endl;
		cout << "Reference : " << chunk.m_reference << endl;
	}
	cout << endl;
	
	return 0;
}
