## lsChunkyDB v0.2-Alpha

A simple trie based encrypted text database system.
Indended for use with text based LLM's as long term memory.

# Features
* Encrypted text data memory system for AI
* Data only decrypted on lookup.
