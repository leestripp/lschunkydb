#include "lsChunkyDB.h"


lsChunkyDB::lsChunkyDB()
{
	m_chunk_id = 0;
	// get user home dir for : ~/.lsChunkyDB/data/data_path
	m_home_dir = getpwuid( getuid() )->pw_dir;
	
	// headers
	m_max_header_size = 80;
}

lsChunkyDB::~lsChunkyDB()
{
	// clear but leave data on disk.
	cleanup();
}

// ************************
// Init

int lsChunkyDB::init( const string& userid, const string& data_path )
{
	// set userid.
	m_userid = userid;
	// Set data path
	m_datapath = data_path;
	
	// Make sure our database folders exist.
	filesystem::path p( m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath );
	if(! filesystem::exists( p ) )
	{
		cout << "Creating database folder : " << m_home_dir << "/.lsChunkyDB/data/" << get_exename() + "/" + m_datapath << endl;
		
		// bool create_directories( const std::filesystem::path& p );
		filesystem::create_directories( m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath );
	}
	
	// check encryption key.
	if(! m_lsgpg.key_exists( m_userid ) )
	{
		// key not found.
		// Create key without passphrase.
		m_lsgpg.gen_key( m_userid, "default", false );
		
		// delte any old data files that will no longer work.
		delete_data();
	}
	
	// Load database.
	cleanup();
	for( const auto& entry : filesystem::directory_iterator( m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath ) )
	{
		// decrypt file
		vector<char> data = m_lsgpg.decrypt_file( entry.path() );
		if(! data.empty() )
		{
			reinsert( string( data.begin(), data.end() ) );
		}
	}
	
	// info
	cout << "Init lsChunkyDB : ~/.lsChunkyDB/data/" << get_exename() << "/" << m_datapath << endl;
	cout << "Chunk count : " << m_chunks.size() << endl;
	
	return 0;
}

bool lsChunkyDB::empty()
{
	if( m_chunk_id ) return false;
	return true;
}

// ************************
// find_chunks

vector<lsChunk> lsChunkyDB::find_chunks( const string& prompt, ulong num_chunks )
{
	vector<lsChunk> result;
	vector<ulong> all_chunk_ids;
	
	// Split into words.
	vector<string> list;
	list = m_trie.regex_split( prompt );
	string prev_word;
	string tripple_word;
	for( const string& word : list )
	{
		vector<ulong> wordpair_ids;
		vector<ulong> wordtripple_ids;
		
		if(! tripple_word.empty() )
		{
			// debug
			// cout << "findChunkIds (tripple): " << tripple_word + prev_word + word << endl;
			wordtripple_ids = m_trie.findChunkIds( tripple_word + prev_word + word );
		}
		if(! prev_word.empty() )
		{
			// debug
			// cout << "findChunkIds (pair): " << prev_word + word << endl;
			wordpair_ids = m_trie.findChunkIds( prev_word + word );
			tripple_word = prev_word;
		}
		prev_word = word;
		
		// append ids.
		if(! wordpair_ids.empty() ) all_chunk_ids.insert( all_chunk_ids.end(), wordpair_ids.begin(), wordpair_ids.end() );
		if(! wordtripple_ids.empty() ) all_chunk_ids.insert( all_chunk_ids.end(), wordtripple_ids.begin(), wordtripple_ids.end() );
		
	} // for words.
	
	// Check it.
	if( all_chunk_ids.empty() )
	{
		// debug
		// cout << "findChunkIds all_chunk_ids empty." << endl;
		return result;
	}
	
	// Check for single ID
	if( all_chunk_ids.size() == 1 )
	{
		result.push_back( m_chunks[ all_chunk_ids[0] ] );
		return result;
	}
	
	// score word counts.
	map<ulong, ulong> id_count;
	// debug
	// cout << "All chunk ids :";
	for( auto aid : all_chunk_ids )
	{
		// debug
		// cout << " " << aid;
		
		if( id_count.find( aid ) == id_count.end() )
		{
			// not found. add new.
			id_count.insert( make_pair( aid, 1 ) );
			
		} else
		{
			// found. increment.
			id_count[ aid ] += 1;
		}
	}
	// debug
	// cout << endl;
	
	// copy into pairs for sorting.
	vector<pair<ulong, ulong>> pairs;
	map<ulong, ulong>::iterator it;
	for( it = id_count.begin(); it != id_count.end(); it++ )
	{
		// Only add if count is grater then 1
		if( it->second > 1 )
		{
			pairs.push_back( *it );
			// debug.
			// cout << "id: " << it->first << "; value = " << it->second << endl;
		}
	}
	sort( pairs.begin(), pairs.end(), [=](pair<ulong, ulong>& a, pair<ulong, ulong>& b)
		{ return a.second > b.second; } );
	// debug
	// cout << endl;
	
	// Add chunks to result, limited by num_chunks and score.
	// num_chunks
	ulong c = 1;
	for( const auto& pair : pairs )
	{
		// debug
		// cout << "Found Chunk with: Chunk id: " << pair.first << ", score = " << pair.second << endl;
		
		if( c > num_chunks ) break;
		
		if( pair.second > 0 )
		{
			result.push_back( m_chunks[ pair.first ] );
			c++;
		}
	}
	cout << endl;
	
	return result;
}

//*******************
// decrypt_chunk

string lsChunkyDB::decrypt_chunk( const lsChunk& chunk, bool remove_header )
{
	// debug
	// cout << "lsChunkyDB::decrypt_chunk: Start." << endl;
	
	vector<char> result;
	
	// Load data
	string filename = m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath + "/chunk_" + to_string( chunk.m_id );
	// decrypt file
	result = m_lsgpg.decrypt_file( filename );
	
	if(! remove_header  )
	{
		return string( result.begin(), result.end() );
	}
	
	// debug
	cout << "lsChunkyDB::decrypt_chunk: Removing Header." << endl;
	// Remove header.
	string data( result.begin(), result.end() );
	stringstream input( data );
	string output;
	ulong i = 0;
	for( string line; getline( input, line ); )
	{
		if( i > 0 ) output += line + "\n";
		i++;
	}
	return output;
}

//*******************
// encrypt_chunk

void lsChunkyDB::encrypt_chunk( const lsChunk& chunk, const string& data )
{
	// debug
	// cout << "lsChunkyDB::encrypt_chunk: Start." << endl;
	
	vector<char> buff( data.begin(), data.end() );
	if( buff.empty() )
	{
		cerr << "ERROR: lsChunkyDB::encrypt_chunk: Saving chunk to file." << endl;
		return;
	}
	
	// debug
	// cout << "Data: " << data << endl;
	
	// save data
	string filename = m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath + "/chunk_" + to_string( chunk.m_id );
	// encrypt data to file
	m_lsgpg.encrypt_file( filename, buff, m_userid );
}

// ****************
// Split

vector<string> lsChunkyDB::split_content( const string& content, const string& header, ulong chunk_size )
{
	// debug
	// cout << "lsChunkyDB::split_content() Called..." << endl;
	
	// Split by chunk_size
	vector<string> chunks;
	string chunk;
	stringstream input( content );
	
	// Split into chunks.
	for( string line; getline( input, line ); )
	{
		// debug
		// cout << line << endl;
		
		// Add line to chunk. Avoid double header on first chunk.
		if( line != header )
		{
			chunk += line + "\n";
		}
		
		// Add chunk.
		if( chunk.size() > chunk_size )
		{
			if( chunk.size() > chunk_size*2 )
			{
				// warning
				cout << "### WARNING : lsChunkyDB::split_content - Chunk size       : " << chunk.size() << endl;
			}
			chunks.push_back( header + "\n" + chunk );
			chunk = "";
		}
	}
	
	// Check last chunk. Avoid tiny chunks.
	if(! chunk.empty() && chunk.size() > 10 )
	{
		if( chunk.size() > chunk_size*2 )
		{
			// warning
			cout << "### WARNING : lsChunkyDB::split_content - Chunk size       : " << chunk.size() << endl;
		}
		// Add chunk.
		chunks.push_back( header + "\n" + chunk );
	}
	
	// debug
	// cout << "lsChunkyDB::split_content - Number of Chunks : " << chunks.size() << endl;
	
	return chunks;
}


// ****************************
// Insert
// TODO: For now if the chunk is to large. I just cut the end off.

void lsChunkyDB::insert( const string& data, ulong chunk_size )
{
	if( data.empty() ) return;
	
	// debug
	cout << "lsChunkyDB::insert() : Chunk size = " << chunk_size << endl;
	
	// lock the mutex
	m_insert_mutex.lock();
	
	// Find header.
	stringstream input( data );
	string header;
	for( string line; getline( input, line ); )
	{
		if(! line.empty() )
		{
			// limit headers to m_max_header_size chars
			if( line.size() > m_max_header_size )
			{
				header = line.substr( 0, m_max_header_size );
			} else header = line;
			header = "HEADER: " + header;
			
			// debug
			// cout << "Header : " << header << endl;
			// cout << "\tData size : " << data.size();
			// cout << endl;
			break;
		}
	}
	
	if( data.size() <= chunk_size )
	{
		// debug
		cout << "Adding data (single) ID : " << m_chunk_id << ", data size : " << data.size() << endl;
		
		// Add chunk.
		lsChunk chunk_data;
		chunk_data.m_id = m_chunk_id;
		chunk_data.m_reference = header;
		// encrypt data to file.
		// void encrypt_chunk( const lsChunk& chunk, const string& data );
		encrypt_chunk( chunk_data, header + "\n" + data );
		
		m_chunks.insert( make_pair( m_chunk_id, chunk_data ) );
		
		// Split into words.
		vector<string> list;
		list = m_trie.regex_split( data );
		insert_wordlist( list, m_chunk_id );
		// next chunk
		m_chunk_id++;
		
	} else
	{
		// split into chunks.
		vector<string> chunks;
		chunks = split_content( data, header, chunk_size );
		for( const string& chunk : chunks )
		{
			// chunk could still be to large. chop it if it is.
			string final_chunk = chunk;
			if( chunk.size() > chunk_size )
			{
				final_chunk = chunk.substr( 0, chunk_size );
			}
			
			// debug
			cout << "Adding data chunk ID : " << m_chunk_id << ", chunk size : " << final_chunk.size() << endl;
			
			// Add chunk.
			lsChunk chunk_data;
			chunk_data.m_id = m_chunk_id;
			chunk_data.m_reference = header;
			// encrypt data to file.
			// void encrypt_chunk( const lsChunk& chunk, const string& data );
			encrypt_chunk( chunk_data, header + "\n" + final_chunk );
			
			m_chunks.insert( make_pair( m_chunk_id, chunk_data ) );
			
			// Split into words for trie.
			vector<string> list;
			list = m_trie.regex_split( final_chunk );
			insert_wordlist( list, m_chunk_id );
			// next chunk
			m_chunk_id++;
			
		} // for loop
	}
	
	// info
	cout << "Insert : ~/.lsChunkyDB/data/" << get_exename() << "/" << m_datapath << endl;
	cout << "Chunk count : " << m_chunks.size() << endl;
	
	// unlock
	m_insert_mutex.unlock();
}

void lsChunkyDB::insert_wordlist( const vector<string>& list, ulong chunk_id )
{
	string prev_word;
	string tripple_word;
	for( const string& word : list )
	{
		// Add word tripple, to improve find.
		if(! tripple_word.empty() )
		{
			m_trie.insert( tripple_word + prev_word + word, chunk_id );
		}
		// Add word pair, to improve find.
		if(! prev_word.empty() )
		{
			m_trie.insert( prev_word + word, chunk_id );
			tripple_word = prev_word;
		}
		prev_word = word;
	}
}


//************
// reinsert
// HEADER: is now imbeded in the chunk on disk.

void lsChunkyDB::reinsert( const string& data )
{
	if( data.empty() ) return;
	
	// lock the mutex
	m_insert_mutex.lock();
	
	// Find header.
	stringstream input( data );
	string header;
	for( string line; getline( input, line ); )
	{
		if(! line.empty() )
		{
			// limit headers to m_max_header_size chars
			// This should not happen, just being safe.
			if( line.size() > m_max_header_size )
			{
				header = line.substr( 0, m_max_header_size );
			} else header = line;
			
			// debug
			// cout << "Header : " << header << endl;
			// cout << "  Data size : " << data.size();
			// cout << endl;
			break;
		}
	}
	
	// debug
	// cout << "Reinsert data (from file) ID : " << m_chunk_id << ", data size : " << data.size() << endl;
	
	// Add chunk.
	lsChunk chunk_data;
	chunk_data.m_id = m_chunk_id;
	chunk_data.m_reference = header;
	
	m_chunks.insert( make_pair( m_chunk_id, chunk_data ) );
	
	// Split into words.
	vector<string> list;
	list = m_trie.regex_split( data );
	insert_wordlist( list, m_chunk_id );
	// next chunk
	m_chunk_id++;
	
	// unlock
	m_insert_mutex.unlock();
}

void lsChunkyDB::delete_data()
{
	string data_folder( m_home_dir + "/.lsChunkyDB/data/" + get_exename() + "/" + m_datapath );
	cout << "Deleting data from : " << data_folder << endl;
	
	// delete all data files.
	for( const auto& entry : filesystem::directory_iterator( data_folder ) )
	{
		filesystem::remove( entry.path() );
	}
	cleanup( true );
}

void lsChunkyDB::cleanup( bool reset_id )
{
	// debug
	// cout << "lsChunkyDB::cleanup() Called..." << endl;
	
	m_chunks.clear();
	m_trie.clearTree();
	if( reset_id ) m_chunk_id = 0;
}

string lsChunkyDB::get_exename()
{
	string exename;
    ifstream( "/proc/self/comm" ) >> exename;
	return exename;
}

