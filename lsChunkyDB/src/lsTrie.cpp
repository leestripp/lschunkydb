#include <filesystem>

#include "lsTrie.h"

lsTrie::lsTrie()
{
	m_to_lower = true;
	root = NULL;
}

void lsTrie::insert( const string& word, ulong chunk_id )
{
	if( word.empty() ) return;
	
	// debug
	// cout << "Insert Word: " << word << ", Chunk id : " << chunk_id << endl;
	
	if(! root ) root = new trieNode();
	trieNode *tmp = root;
	for( ulong i=0; i<word.length(); i++ )
	{
		if( tmp->node.find( word[i] ) == tmp->node.end() )
		{
			// No letter found, so store it and add new Node.
			tmp->node[ word[i] ] = new trieNode();
		}
		// next node.
		tmp = tmp->node[ word[i] ];
    }
    // whole word added or exists.
	tmp->isWord = true;
	tmp->m_chunk_ids.push_back( chunk_id );
}

void lsTrie::iterate_( const trieNode* trienode, const std::string& prefix )
{
	if( trienode->isWord )
	{
		// debug
		// cout << prefix << endl;
		// cout << ", chunk id count : " << trienode->m_chunk_ids.size() << endl;
		
		m_wordlist.push_back( prefix );
	}
	
	for( const auto& [c, child] : trienode->node )
	{
		iterate_( child, prefix + c );
	}
}

void lsTrie::iterate()
{
	if( root )
	{
		// debug
		// cout << "Find all Words" << endl;
		
		m_wordlist.clear();
		iterate_( root, "" );
	} else
	{
		cout << "Tree empty..." << endl;
	}
}

void lsTrie::findPrefix_( const trieNode* node, const string& prefix, ulong pos )
{
	trieNode *tmp;
	
	if( auto search = node->node.find( prefix[pos] ); search != node->node.end() )
	{
		tmp = search->second;
		
		if( prefix.size() == pos+1 )
		{
			found = tmp;
			return;
		} else
		{
			pos++;
			findPrefix_( tmp, prefix, pos );
		}
	}
}


vector<string> lsTrie::findPrefix( const string& prefix )
{
	found = NULL;
	ulong pos = 0;
	
	if( root )
	{
		// debug
		// cout << "Find words with prefix: " << prefix << endl;
		findPrefix_( root, prefix, pos );
		
		// display words from node found.
		if( found )
		{
			// clear word list
			m_wordlist.clear();
			// Add new words
			iterate_( found, prefix );
		}
	} else
	{
		// Clear word list
		m_wordlist.clear();
	}
	
	return m_wordlist;
}

vector<ulong> lsTrie::findChunkIds( const string& word )
{
	// debug
	// cout << "lsTrie::findChunkIds Called..." << endl;
	
	// clear id list
	m_idlist.clear();
	found = NULL;
	ulong pos = 0;
	
	if( root )
	{
		// debug
		// cout << "Find word: " << word << endl;
		findPrefix_( root, word, pos );
		
		// Word found ?
		if( found )
		{
			// Add ids.
			m_idlist = found->m_chunk_ids;
			// debug
			// cout << "lsTrie::findChunkIds Word found : id count : " << m_idlist.size() << endl;
		}
	}
	return m_idlist;
}

void lsTrie::clear_( const trieNode* trienode )
{
	for( const auto& [c, child] : trienode->node )
	{
		// debug
		// cout << " " << c;
		clear_( child );
		delete child;
	}
}

void lsTrie::clearTree()
{
	if( root )
	{
		// debug
		// cout << "clearNodes:";
		clear_( root );
		delete root;
		// cout << endl;
	}
	root = NULL;
}

vector<string> lsTrie::regex_split( const string &s, const regex& sep_regex )
{
	string data = s;
	
	if( m_to_lower )
	{
		transform( data.begin(), data.end(), data.begin(), []( unsigned char c ){ return std::tolower(c); });
	}
	
	sregex_token_iterator iter( data.begin(), data.end(), sep_regex, -1 );
	sregex_token_iterator end;
	
	return {iter, end};
}


