#ifndef LSTRIE_H
#define LSTRIE_H

#include <iostream>
#include <sstream>
#include <unordered_map>
#include <regex>
#include <mutex>

using namespace std;

// Data
class trieNode
{
public:
	unordered_map<char, trieNode*> node;
	
	bool isWord;
	vector<ulong> m_chunk_ids;
};

// Base Manager
class lsTrie
{
public:
	lsTrie();
	
	// trie tree
	void insert( const string& word, ulong chunk_id );
	void iterate();
	void clearTree();
	vector<string> findPrefix( const string& prefix );
	vector<ulong> findChunkIds( const string& word );
	// Strings
	vector<string> regex_split( const string &s, const regex& sep_regex = regex{"[\\s.,&?!\\(\\);:\\{\\}\"\t=\\-\\+#|$\\*\\[\\]/<>]+"} );
	
private:
	void iterate_( const trieNode* node, const string& prefix );
	void clear_( const trieNode* trienode );
	void findPrefix_( const trieNode* node, const string& prefix, ulong pos );
	
	trieNode *root;
	trieNode *found;
	vector<string> m_wordlist;
	vector<ulong> m_idlist;
	bool m_to_lower;
};

#endif // LSTRIE_H

