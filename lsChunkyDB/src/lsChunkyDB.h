#ifndef LSCHUNKYDB_H
#define LSCHUNKYDB_H

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <map>
#include <fstream>
#include <filesystem>
#include <mutex>
// user home
#include <unistd.h>
#include <pwd.h>
// exe name
#include <stdio.h>

// GPG
#include <lsGPG/lsGPG.h>

// Trie
#include "lsTrie.h"

using namespace std;

class lsChunk
{
public:
	ulong		m_id;
	string		m_reference;
};

class lsChunkyDB
{
public:
	lsChunkyDB();
	virtual ~lsChunkyDB();
	
	//init
	int init( const string& userid, const string& data_path );
	// find
	vector<lsChunk> find_chunks( const string& prompt, ulong num_chunks );
	// encrypt, decrypt.
	string decrypt_chunk( const lsChunk& chunk, bool remove_header=false );
	void encrypt_chunk( const lsChunk& chunk, const string& data );
	// checks
	bool empty();
	
	// add data.
	void insert( const string& data, ulong chunk_size );
	// remove data
	void cleanup( bool reset_id=false );
	void delete_data();
	// utils
	string get_exename();
	
	void set_datapath( const string& val )
	{
		m_datapath = val;
	}
	
private:
	vector<string> split_content( const string& content, const string& header, ulong chunk_size );
	// reinsert
	void reinsert( const string& data );
	void insert_wordlist( const vector<string>& list, ulong chunk_id );
	
	// data
	lsTrie m_trie;
	lsGPG m_lsgpg;
	map<ulong, lsChunk> m_chunks;
	ulong m_chunk_id;
	ulong m_max_header_size;
	// threads.
	mutex m_insert_mutex;
	
	// GPG key id
	string m_userid;
	// home dir
	string m_home_dir;
	// data path.
	string m_datapath;
};

#endif // LSCHUNKYDB_H